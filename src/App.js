import React, { Component } from "react";
import "./App.scss";
import NavBar from "./components/sub/NavBar";
import Footer from "./components/sub/Footer";
import Home from "./components/Home";
import About from "./components/About";
import BdsBan from "./components/BdsBan";
import BdsBanDetails from "./components/BdsBanDetails";
import Contact from "./components/Contact";
import TuyenDung from "./components/TuyenDung";
import BdsThue from "./components/BdsThue";

class App extends Component {
  render() {
    return (
      <>
        <NavBar />
        {/* <Home /> */}
        {/* <About /> */}
        {/* <BdsBan /> */}
        <BdsBanDetails />
        {/* <Contact /> */}
        {/* <BdsThue /> */}
        {/* <TuyenDung /> */}
        <Footer />
      </>
    );
  }
}

export default App;
