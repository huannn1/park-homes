import React, { Component } from "react";

class NavBar extends Component {
  render() {
    return (
      <>
        <nav
          class="navbar navbar-expand-lg navbar-light fixed-top"
          id="mainNav"
        >
          <div class="container">
            <a class="navbar-brand mr-md-5" href="#">
              <img src="img/logo.png" />
            </a>
            <button
              class="navbar-toggler navbar-toggler-right"
              type="button"
              data-toggle="collapse"
              data-target="#navbarResponsive"
              aria-controls="navbarResponsive"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="fa fa-bars"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link js-scroll-trigger" href="#about">
                    <i class="fa fa-home" />
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#services">
                    Giới thiệu
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#portfolio">
                    BĐS bán
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#contact">
                    BĐS thuê
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#contact">
                    Thiết kế nội thất
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#contact">
                    Tin tức
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#contact">
                    Tuyển dụng
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#contact">
                    Liên hệ
                  </a>
                </li>
              </ul>
              <ul className="navbar-nav ml-auto">
                <li className="nav-item nav-item--hotline d-none d-lg-block">
                  <div className="nav-text">
                    Hotline
                  </div>
                </li>
                <li className="nav-item nav-item--contact text-center">
                  <h3 className="mb-1"><a className="contact-link" href="tel:0901719699">0901719699</a></h3>
                  <div><a className="contact-link" href="mailto:someone@yoursite.com">linhnt@parkhomes.vn</a></div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </>
    );
  }
}

export default NavBar;
