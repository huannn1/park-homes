import React, { Component } from "react";
import PrimaryHeader from "./sub/PrimaryHeader";
import PageIntro from "./sub/PageIntro";
import Projects from "./sub/Projects";
import Contacts from "./sub/Contacts";
import Partners from "./sub/Partners";

export default class extends Component {
  componentDidMount() {
    
  }

  render() {
    return (
      <>
        <PrimaryHeader />
        <PageIntro />
        <Projects />
        <Contacts />
        <Partners />
      </>
    );
  }
}
