import React, { Component } from "react";
import SecondaryHeader from "./sub/SecondaryHeader";
import PageIntro from "./sub/PageIntro";
import Projects from "./sub/Projects";
import Contacts from "./sub/Contacts";
import Partners from "./sub/Partners";

export default class extends Component {
  render() {
    return (
      <>
        <SecondaryHeader />
        <PageIntro />

        <section className="media-grid">
          <div className="mb-5">
            <div className="bg-primary">
              <div className="container">
                <div className="row no-gutters">
                  <div className="col-md-6 bg-primary p-3 p-md-5 text-white">
                    <h2 class="text-uppercase mb-3">Giới thiệu</h2>
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s, when an unknown
                      printer took a galley of type and scrambled it to make a type
                      specimen book. It has survived not only five centuries, but
                      also the leap into electronic typesetting, remaining
                      essentially unchanged. It was popularised in the 1960s with
                      the release of Letraset sheets containing Lorem Ipsum
                      passages, and more recently with desktop publishing software
                      like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                  </div>
                  <div className="col-md-6 bg-white media-grid__img" style={{backgroundImage: "url('img/misc/banner-home.jpg')"}}>
                  </div>
                </div>
            </div>
            </div>

            <div className="bg-white">
              <div className="container">
                <div className="row no-gutters">
                  <div className="col-md-6 bg-white media-grid__img order-md-1 order-2" style={{backgroundImage: "url('img/misc/banner-home.jpg')"}}>
                  </div>
                  <div className="col-md-6 bg-white p-3 p-md-5 order-md-2 order-1">
                    <h2 class="text-uppercase mb-3">Giới thiệu</h2>
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s, when an unknown
                      printer took a galley of type and scrambled it to make a type
                      specimen book. It has survived not only five centuries, but
                      also the leap into electronic typesetting, remaining
                      essentially unchanged. It was popularised in the 1960s with
                      the release of Letraset sheets containing Lorem Ipsum
                      passages, and more recently with desktop publishing software
                      like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div className="bg-secondary">
              <div className="container">
                <div className="row no-gutters">
                  <div className="col-md-6 bg-secondary p-3 p-md-5 text-white ">
                    <h2 class="text-uppercase mb-3">Giới thiệu</h2>
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s, when an unknown
                      printer took a galley of type and scrambled it to make a type
                      specimen book. It has survived not only five centuries, but
                      also the leap into electronic typesetting, remaining
                      essentially unchanged. It was popularised in the 1960s with
                      the release of Letraset sheets containing Lorem Ipsum
                      passages, and more recently with desktop publishing software
                      like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                  </div>
                  <div className="col-md-6 bg-white media-grid__img" style={{backgroundImage: "url('img/misc/banner-home.jpg')"}}>
                  </div>
                </div>
            </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}
