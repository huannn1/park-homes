import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
        <section id="mast-contacts" class="mast-contacts text-white">
          <div class="container">
            <div class="row">
              <div class="col-lg-4 col-md-4 text-center">
                <div class="p-4">
                  <img
                    src="img/icons/location-white.png"
                    class="mast-contact__img mb-2"
                  />
                  <h3 class="mb-3">Địa điểm</h3>
                  <p class="">
                    Our templates are updated regularly so they don't break.
                  </p>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 text-center">
                <div class="p-4">
                  <img
                    src="img/icons/phone-white.png"
                    class="mast-contact__img mb-2"
                  />
                  <h3 class="mb-3">Điện thoại</h3>
                  <a className="contact-link text-white" href="tel:0901719699">0901719699</a>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 text-center">
                <div class="p-4">
                  <img
                    src="img/icons/email-white.png"
                    class="mast-contact__img mb-2"
                  />
                  <h3 class="mb-3">Email</h3>
                  <a className="text-white" href="mailto:someone@yoursite.com">linhnt@parkhomes.vn</a>
                </div>
              </div>
            </div>
          </div>
        </section>
    );
  }
}