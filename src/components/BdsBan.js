import React, { Component } from "react";
import SecondaryHeader from "./sub/SecondaryHeaderWithSearch";

export default class extends Component {
  render() {
    return (
      <>
        <SecondaryHeader />

        <div className="py-5 mt-5">
          <div className="mb-4">
            <div className="container">
              <div className="row">
                <div className="col-md-4">
                  <div class="content-box">
                    <div
                      class="content-box__img"
                      style={{
                        backgroundImage: "url('img/projects/02.jpg')"
                      }}
                    />
                    <div class="content-box__text m-4">
                      <h4>Title 01</h4>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry
                    </div>
                  </div>
                </div>

                <div className="col-md-4">
                  <div class="content-box">
                    <div
                      class="content-box__img"
                      style={{
                        backgroundImage: "url('img/projects/02.jpg')"
                      }}
                    />
                    <div class="content-box__text m-4">
                      <h4>Title 02</h4>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry
                    </div>
                  </div>
                </div>

                <div className="col-md-4">
                  <div class="content-box">
                    <div
                      class="content-box__img"
                      style={{
                        backgroundImage: "url('img/projects/02.jpg')"
                      }}
                    />
                    <div class="content-box__text m-4">
                      <h4>Title 03</h4>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <nav aria-label="...">
            <ul class="pagination pagination-lg justify-content-center">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">1</a>
              </li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
            </ul>
          </nav>
        </div>
      </>
    );
  }
}
