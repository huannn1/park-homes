import React, { Component } from "react";
import SecondaryHeader from "./sub/SecondaryHeaderWithSearch";

export default class extends Component {
  render() {
    return (
      <>
        <SecondaryHeader />

        <section className="bds-thue">
          <div className="container">
            <div className="py-5 mt-5">
              <div>
                <div class="content-box content-box--flex mb-5">
                  <div
                    class="content-box__img"
                    style={{
                      backgroundImage: "url('img/projects/02.jpg')"
                    }}
                  >
                    <div class="content-box__img-caption">
                      <h4 className="mb-1 text-uppercase">Căn hộ</h4>
                      <p>Description</p>
                    </div>
                  </div>
                  <div class="content-box__text m-4">
                    <h4>Title 01</h4>
                    <p>
                    *The maximum size limit for file upload is 2 megabytes. All files bigger than 500k will be formatted to a new window for performance reason and to prevent your browser from being unresponsive.
                    </p>
                  </div>
                </div>

                <div class="content-box content-box--flex mb-5">
                  <div
                    class="content-box__img"
                    style={{
                      backgroundImage: "url('img/projects/02.jpg')"
                    }}
                   >
                    <div class="content-box__img-caption">
                      <h4 className="mb-1 text-uppercase">Căn hộ</h4>
                      <p>Description</p>
                    </div>
                  </div>
                  <div class="content-box__text m-4">
                    <h4>Title 01</h4>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry
                  </div>
                </div>

                <div class="content-box content-box--flex mb-5">
                  <div
                    class="content-box__img"
                    style={{
                      backgroundImage: "url('img/projects/02.jpg')"
                    }}
                  >
                    <div class="content-box__img-caption">
                      <h4 className="mb-1 text-uppercase">Căn hộ</h4>
                      <p>Description</p>
                    </div>
                  </div>
                  <div class="content-box__text m-4">
                    <h4>Title 01</h4>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry
                  </div>
                </div>
              </div>

              <nav aria-label="...">
                <ul class="pagination pagination-lg justify-content-center">
                  <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">1</a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                </ul>
              </nav>
            </div>
          </div>
        </section>
      </>
    );
  }
}
