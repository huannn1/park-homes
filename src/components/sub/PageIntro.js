import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
      <>
        <section id="page-intro" class="page-intro">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-md-6 d-md-block py-5 py-md-0 pr-md-5 align-self-start">
                <img
                  src="img/misc/home-about.jpg"
                  class="img-fluid page-intro__img"
                />
              </div>

              <div class="col-md-6 p-md-5">
                <h2 class="text-uppercase text-primary mb-3">Giới thiệu</h2>
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged. It was popularised in the 1960s with
                  the release of Letraset sheets containing Lorem Ipsum
                  passages, and more recently with desktop publishing software
                  like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}