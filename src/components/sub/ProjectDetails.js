import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
        <div class="project-details mb-5">
            <div class="container">
                <ul class="project-details__tabs nav nav-pills nav-justified" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-location-tab" data-toggle="pill" href="#pills-project-location" role="tab" aria-controls="pills-home" aria-selected="true">VỊ trí</a>
                        <a class="nav-link nav-link--pop">VỊ trí</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-dimensions-tab" data-toggle="pill" href="#pills-project-dimensions" role="tab" aria-controls="pills-profile" aria-selected="false">Diện tích</a>
                        <a class="nav-link nav-link--pop">Diện tích</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-facilities-tab" data-toggle="pill" href="#pills-project-facilities" role="tab" aria-controls="pills-contact" aria-selected="false">Tiện ích</a>
                        <a class="nav-link nav-link--pop">Tiện ích</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-library-tab" data-toggle="pill" href="#pills-project-library" role="tab" aria-controls="pills-contact" aria-selected="false">Thư viện</a>
                        <a class="nav-link nav-link--pop">Thư viện</a>
                    </li>
                </ul>
            </div>

            <div className="bg-primary project-details__content text-white">
                <div class="tab-content" id="pills-tabContent">
                    {/* Vị trí */}
                    <div class="tab-pane fade show active" id="pills-project-location">
                        <div className="container">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <h2 className="text-uppercase">Kết nối tới muôn nơi</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                                <div className="col-md-7">
                                <iframe
                                    width="100%"
                                    height="450"
                                    frameborder="0" style={{border: 0}}
                                    src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2965.0824050173574!2d-93.63905729999999!3d41.998507000000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sWebFilings%2C+University+Boulevard%2C+Ames%2C+IA!5e0!3m2!1sen!2sus!4v1390839289319" allowfullscreen>
                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* Diện tích */}
                    <div class="tab-pane fade" id="pills-project-dimensions">
                        <div className="container">
                            Diện tích
                        </div>
                    </div>

                    {/* Tiện ích */}
                    <div class="tab-pane fade" id="pills-project-facilities">
                        <div className="container project-details__gallery">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="nav flex-column nav-pills project-details__gallery-types" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        <a class="nav-link active project-details__gallery-type" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-park" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                            <img className="project-details__gallery-icon img-fluid" src="img/icons/bbq.png" />
                                            <div>Công viên</div>
                                        </a>
                                        <a class="nav-link project-details__gallery-type" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-health" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                                            <img className="project-details__gallery-icon img-fluid" src="img/icons/heart.png" />
                                            <div>Sức khoẻ</div>
                                        </a>
                                        <a class="nav-link project-details__gallery-type" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-edu" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                                            <img className="project-details__gallery-icon img-fluid" src="img/icons/edu.png" />
                                            <div>Giáo dục</div>
                                        </a>
                                        <a class="nav-link project-details__gallery-type" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-shopping" role="tab" aria-controls="v-pills-settings" aria-selected="false">
                                            <img className="project-details__gallery-icon img-fluid" src="img/icons/shopping.png" />
                                            <div>Shopping</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="tab-content" id="v-pills-tabContent">
                                        <div class="tab-pane fade show active" id="v-pills-park">
                                            <div id="carouselFacilties" class="carousel slide" data-ride="carousel">
                                                <ol class="carousel-indicators">
                                                    <li data-target="#carouselFacilties" data-slide-to="0" class="active"></li>
                                                    <li data-target="#carouselFacilties" data-slide-to="1"></li>
                                                    <li data-target="#carouselFacilties" data-slide-to="2"></li>
                                                </ol>
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                    <img class="d-block w-100" src="img/misc/gallery-01.jpg" alt="First slide" />
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img class="d-block w-100" src="img/misc/gallery-01.jpg" alt="Second slide"/>
                                                    </div>
                                                    <div class="carousel-item">
                                                    <img class="d-block w-100" src="img/misc/gallery-01.jpg" alt="Third slide"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-health">...</div>
                                        <div class="tab-pane fade" id="v-pills-edu">...</div>
                                        <div class="tab-pane fade" id="v-pills-shopping">...</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* Thư viện */}
                    <div class="tab-pane fade" id="pills-project-library">
                        <div className="container">
                            <div id="carouselLibrary" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselLibrary" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselLibrary" data-slide-to="1"></li>
                                    <li data-target="#carouselLibrary" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                    <img class="d-block w-100" src="img/misc/gallery-01.jpg" alt="First slide" />
                                    </div>
                                    <div class="carousel-item">
                                    <img class="d-block w-100" src="img/misc/gallery-01.jpg" alt="Second slide"/>
                                    </div>
                                    <div class="carousel-item">
                                    <img class="d-block w-100" src="img/misc/gallery-01.jpg" alt="Third slide"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}