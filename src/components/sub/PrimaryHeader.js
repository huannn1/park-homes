import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
      <>
        <header class="masthead masthead--home text-white d-flex">
          <div class="container my-auto">
          <div id="carouselExampleIndicators" class="carousel--frame carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active">1</li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1">2</li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2">3</li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <div class="carousel-frame">
                  <div className="carousel-frame__upper animated fadeInDown"></div>
                  <div className="carousel-frame__lower animated fadeInUp"></div>
                  <div className="carousel-caption__big animated fadeInLeft">Parkhomes 1</div>
                  <div className="carousel-caption__small animated fadeInUp">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="carousel-frame">
                  <div className="carousel-frame__upper animated fadeInDown"></div>
                  <div className="carousel-frame__lower animated fadeInUp"></div>
                  <div className="carousel-caption__big animated fadeInLeft">Parkhomes 2</div>
                  <div className="carousel-caption__small animated fadeInUp">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="carousel-frame">
                  <div className="carousel-frame__upper animated fadeInDown"></div>
                  <div className="carousel-frame__lower animated fadeInUp"></div>
                  <div className="carousel-caption__big animated fadeInLeft">Parkhomes 3</div>
                  <div className="carousel-caption__small animated fadeInUp">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </header>
      </>
    );
  }
}