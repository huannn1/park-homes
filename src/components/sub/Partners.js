import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
        <section id="partners" class="partners py-5">
          <div class="container">
            <div class="container-fluid">
              <h2 class="mb-5 text-primary  text-center">
                <span class="section__header">Partners</span>
              </h2>
              <div
                id="carouselPartners"
                class="carousel carousel--multiple slide"
                data-ride="carousel"
                data-interval="9000"
              >
                <div class="carousel-inner row w-100 mx-auto" role="listbox">
                  <div class="carousel-item carousel-item--project col-md-3 active">
                    <div class="partner-box p-4">
                      <div class="partner-box__img">
                        <img src="img/icons/email.png" class="img-fluid" />
                      </div>
                    </div>
                  </div>
                  <div class="carousel-item carousel-item--project col-md-3">
                    <div class="partner-box p-4">
                      <div class="partner-box__img">
                        <img src="img/icons/email.png" class="img-fluid" />
                      </div>
                    </div>
                  </div>
                  <div class="carousel-item carousel-item--project col-md-3">
                    <div class="partner-box p-4">
                      <div class="partner-box__img">
                        <img src="img/icons/email.png" class="img-fluid" />
                      </div>
                    </div>
                  </div>
                  <div class="carousel-item carousel-item--project col-md-3">
                    <div class="partner-box p-4">
                      <div class="partner-box__img">
                        <img src="img/icons/email.png" class="img-fluid" />
                      </div>
                    </div>
                  </div>
                  <div class="carousel-item carousel-item--project col-md-3">
                    <div class="partner-box p-4">
                      <div class="partner-box__img">
                        <img src="img/icons/email.png" class="img-fluid" />
                      </div>
                    </div>
                  </div>
                  <div class="carousel-item carousel-item--project col-md-3">
                    <div class="partner-box p-4">
                      <div class="partner-box__img">
                        <img src="img/icons/email.png" class="img-fluid" />
                      </div>
                    </div>
                  </div>
                  <div class="carousel-item carousel-item--project col-md-3">
                    <div class="partner-box p-4">
                      <div class="partner-box__img">
                        <img src="img/icons/email.png" class="img-fluid" />
                      </div>
                    </div>
                  </div>
                  <div class="carousel-item carousel-item--project col-md-3">
                    <div class="partner-box p-4">
                      <div class="partner-box__img">
                        <img src="img/icons/email.png" class="img-fluid" />
                      </div>
                    </div>
                  </div>
                </div>
                <a
                  class="carousel-control-prev"
                  href="#carouselPartners"
                  role="button"
                  data-slide="prev"
                >
                  <i class="fa fa-chevron-left fa-lg text-secondary fa-2x" />
                  <span class="sr-only">Previous</span>
                </a>
                <a
                  class="carousel-control-next text-faded"
                  href="#carouselPartners"
                  role="button"
                  data-slide="next"
                >
                  <i class="fa fa-chevron-right fa-lg text-secondary fa-2x" />
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        </section>
    );
  }
}