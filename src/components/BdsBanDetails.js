import React, { Component } from "react";
import SecondaryHeader from "./sub/SecondaryHeader";
import PageIntro from "./sub/PageIntro";
import ProjectDetails from "./sub/ProjectDetails";

export default class extends Component {
  render() {
    return (
      <>
        <SecondaryHeader />
        <PageIntro />
        <ProjectDetails />
      </>
    );
  }
}
