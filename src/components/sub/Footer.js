import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <>
        <footer class="footer py-5 text-white">
          <div class="container">
            <div class="row">
              <div class="col-md-5">
                <div class="row">
                  <div class="col flex-grow-0">
                    <a class="navbar-brand mr-2" href="#page-top">
                      <img src="img/logo.png" />
                    </a>
                  </div>

                  <div class="col flex-grow-1">
                    <h5 class="text-uppercase">
                      Công ty cổ phần Parkhomes Việt Nam
                    </h5>
                    <p>
                      Địa chỉ: Lô 11, BT2 Khu đô thị Tân Tây Đô, xã Tân Lập,
                      huyện Đan Phượng, TP Hà Nội
                    </p>
                  </div>
                </div>
              </div>

              <div class="col-md-7">
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <div class="row align-items-center">
                      <div class="col flex-grow-0 mb-1 mb-md-0">
                        <img
                          src="img/icons/location-white.png"
                          class="mast-contact__img"
                        />
                      </div>
                      <div class="col flex-grow-1 pl-0">
                        <h5 class="text-uppercase m-0">
                          <a className="contact-link text-white" href="tel:0901719699">0901719699</a>
                        </h5>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="row align-items-center">
                      <div class="col flex-grow-0 mb-1 mb-md-0">
                        <img
                          src="img/icons/location-white.png"
                          class="mast-contact__img"
                        />
                      </div>
                      <div class="col flex-grow-1 pl-0">
                        <p class="text-uppercase m-0">
                          <a className="contact-link text-white" href="mailto:someone@yoursite.com">linhnt@parkhomes.vn</a>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </>
    );
  }
}

export default Footer;
