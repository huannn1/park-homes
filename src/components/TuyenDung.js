import React, { Component } from "react";
import SecondaryHeader from "./sub/SecondaryHeaderWithSearch";

export default class extends Component {
  render() {
    return (
      <>
        <SecondaryHeader />

        <div className="py-5 mt-5">
          <div className="mb-4">
            <div className="container">
              <div className="row">
                <div className="col-md-4">
                  <div class="content-box mb-5" data-toggle="modal" data-target="#detailsModal">
                    <div
                      class="content-box__img"
                      style={{
                        backgroundImage: "url('img/misc/tuyendung-01.jpg')"
                      }}
                    />
                    <div class="content-box__text m-4">
                      <h4>Title 01</h4>
                      <div>Số lượng: 10</div>
                      <div>Hạn cuối: 10/10/2018</div>
                    </div>
                  </div>
                </div>

                <div className="col-md-4">
                  <div class="content-box mb-5" data-toggle="modal" data-target="#detailsModal">
                    <div
                      class="content-box__img"
                      style={{
                        backgroundImage: "url('img/misc/tuyendung-02.jpg')"
                      }}
                    />
                    <div class="content-box__text m-4">
                      <h4>Title 02</h4>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry
                    </div>
                  </div>
                </div>

                <div className="col-md-4" data-toggle="modal" data-target="#detailsModal">
                  <div class="content-box mb-5">
                    <div
                      class="content-box__img"
                      style={{
                        backgroundImage: "url('img/misc/tuyendung-03.jpg')"
                      }}
                    />
                    <div class="content-box__text m-4">
                      <h4>Title 03</h4>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <nav aria-label="...">
            <ul class="pagination pagination-lg justify-content-center">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">1</a>
              </li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
            </ul>
          </nav>
        </div>

        <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  Modal title
                  <div className="modal-title--sub">
                    Sub title here
                  </div>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
              *The maximum size limit for file upload is 2 megabytes. All files bigger than 500k will be formatted to a new window for performance reason and to prevent your browser from being unresponsive.
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
