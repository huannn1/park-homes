import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
      <>
        <header class="masthead text-white">
          <div className="masthead__container container">
            <div class="masthead__title">
              Introduction
            </div>

            <form class="input-group masthead__search ml-auto mt-auto">
              <input type="text" class="form-control masthead__search--input" placeholder="Search" aria-describedby="button-addon2" />
              <div class="input-group-append">
                <button class="btn btn-secondary masthead__search--btn" type="button" id="button-addon2">
                  <i className="fa fa-search" />
                </button>
              </div>
            </form>
          </div>
        </header>
      </>
    );
  }
}