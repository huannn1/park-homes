import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
        <section id="projects" class="projects py-5">
        <div class="container">
          <div class="container-fluid">
            <h2 class="mb-5 text-primary text-center">
              <span class="section__header">Dự án</span>
            </h2>
            <div
              id="carouselProjects"
              class="carousel carousel--multiple slide"
              data-ride="carousel"
              data-interval="9000"
            >
              <div class="carousel-inner row w-100 mx-auto" role="listbox">
                <div class="carousel-item carousel-item--project col-md-3 active">
                  <div class="content-box">
                    <div
                      class="content-box__img"
                      style={{
                        backgroundImage: "url('img/projects/01.jpg')"
                      }}
                    />
                    <div class="content-box__text m-4">
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry
                    </div>
                  </div>
                </div>
                <div class="carousel-item carousel-item--project col-md-3">
                  <div class="content-box">
                    <div
                      class="content-box__img"
                      style={{
                        backgroundImage: "url('img/projects/02.jpg')"
                      }}
                    />
                    <div class="content-box__text m-4">
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry
                    </div>
                  </div>
                </div>
                <div class="carousel-item carousel-item--project col-md-3">
                  <div class="content-box">
                    <div
                      class="content-box__img"
                      style={{
                        backgroundImage: "url('img/projects/03.jpg')"
                      }}
                    />
                    <div class="content-box__text m-4">
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry
                    </div>
                  </div>
                </div>
                <div class="carousel-item carousel-item--project col-md-3">
                  <div class="content-box">
                    <div
                      class="content-box__img"
                      style={{
                        backgroundImage: "url('img/projects/04.jpg')"
                      }}
                    />
                    <div class="content-box__text m-4">
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry
                    </div>
                  </div>
                </div>
                <div class="carousel-item carousel-item--project col-md-3">
                  <div class="content-box">
                    <div
                      class="content-box__img"
                      style={{
                        backgroundImage: "url('img/projects/01.jpg')"
                      }}
                    />
                    <div class="content-box__text m-4">
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry
                    </div>
                  </div>
                </div>
              </div>
              <a
                class="carousel-control-prev"
                href="#carouselProjects"
                role="button"
                data-slide="prev"
              >
                <i class="fa fa-chevron-left fa-lg text-secondary fa-2x" />
                <span class="sr-only">Previous</span>
              </a>
              <a
                class="carousel-control-next text-faded"
                href="#carouselProjects"
                role="button"
                data-slide="next"
              >
                <i class="fa fa-chevron-right fa-lg text-secondary fa-2x" />
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
        </div>
      </section>
    );
  }
}