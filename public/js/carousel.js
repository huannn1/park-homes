$('.carousel--multiple').on('slide.bs.carousel', function (e) {
    var $e = $(e.relatedTarget, this);
    var idx = $e.index();
    var itemsPerSlide = 4;
    var totalItems = $('.carousel-item', this).length;
    var inner = $('.carousel-inner', this);
    
    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item', this).eq(i).appendTo(inner);
            }
            else {
                $('.carousel-item', this).eq(0).appendTo(inner);
            }
        }
    }
});